This library has reached its end-of-life. Please see [edowd/libcoreaws3](https://bitbucket.org/edowd/libcoreaws3) for the current version.
