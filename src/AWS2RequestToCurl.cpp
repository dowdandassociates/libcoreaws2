/*
 *
 * libcoreaws2/src/AWS2RequestToCurl.cpp
 *
 *-------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 *
 */

#include <iostream>
#include <string>

#include "common.hpp"

int main(int argc, char** argv)
{
    coreaws::SignerHandle signer(new coreaws::AWS2Signer());
    coreaws::AWSCredentialsHandle credentials = readCredentials();

    coreaws::Method method = coreaws::MethodUtils::POST;
    coreaws::Scheme scheme = coreaws::SchemeUtils::HTTPS;
    std::string host = "ec2.amazonaws.com";
    coreaws::port_t port = coreaws::Port::DEFAULT;
    std::string path = "/";
    coreaws::ParameterMap parameters;
    parameters.insert(coreaws::ParameterPair("Version", "2012-03-01"));
    parameters.insert(coreaws::ParameterPair("Action", "DescribeAvailabilityZones"));
    std::string fragment = "";
    coreaws::HeaderMap headers;
    coreaws::InputHandle inputStream = coreaws::StreamUtils::emptyInputStream();
    coreaws::RequestHandle request(new coreaws::Request(method, scheme, host, port, path, parameters, fragment, headers, inputStream));
    
    std::cout << requestToCurl(signer, credentials, request) << std::endl;

    return 0;
}

