/*
 *
 * libcoreaws2/src/AWS4RequestTest.cpp
 *
 *-------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 *
 */

#include <iostream>
#include <sstream>
#include <string>

#include <curl/curl.h>

#include "common.hpp"

int main(int argc, char** argv)
{
    try
    {
    coreaws::Connection::init();

    coreaws::SignerHandle signer(new coreaws::AWS4Signer());
    coreaws::AWSCredentialsHandle credentials = readCredentials();

    coreaws::Method method = coreaws::MethodUtils::POST;
    coreaws::Scheme scheme = coreaws::SchemeUtils::HTTPS;
    std::string host = "iam.amazonaws.com";
    coreaws::port_t port = coreaws::Port::DEFAULT;
    std::string path = "/";
    coreaws::ParameterMap parameters;
    parameters.insert(coreaws::ParameterPair("Version", "2010-05-08"));
    parameters.insert(coreaws::ParameterPair("Action", "GetUser"));
    std::string fragment = "";
    coreaws::HeaderMap headers;
    coreaws::InputHandle inputStream = coreaws::StreamUtils::emptyInputStream();
    coreaws::RequestHandle request(new coreaws::Request(method, scheme, host, port, path, parameters, fragment, headers, inputStream));
    
    coreaws::ResponseHandle response = coreaws::Connection::execute(signer, credentials, request);

    CURLcode rc = response->returnCode;
    if (rc != CURLE_OK)
    {
        std::cerr << "Error: " << response->errorMessage << " [" << curl_easy_strerror(rc) << "] (" << rc << ')' << std::endl;
    }

    coreaws::OutputHandle outputStream = response->output;
    std::tr1::shared_ptr<std::stringstream> output = std::tr1::static_pointer_cast<std::stringstream>(outputStream);
    std::cout << output->str() << std::endl;

    coreaws::Connection::cleanup();
    return 0;
    }
    catch (std::exception e)
    {
        std::cerr << "Exception: " << e.what() << std::endl;
        return 1;
    }
    catch (...)
    {
        std::cerr << "Unknown exception" << std::endl;
        return 1;
    }

}

