/*
 *
 * libcoreaws2/src/S3RequestTest.cpp
 *
 *-------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 *
 */

#include <iostream>
#include <sstream>
#include <string>

#include <curl/curl.h>

#include "common.hpp"

int main(int argc, char** argv)
{
    try
    {
    coreaws::Connection::init();

    std::cerr << "Got Here 1" << std::endl;

    coreaws::SignerHandle signer(new coreaws::S3Signer("dowdandassociates-test", "test x.txt"));
    coreaws::AWSCredentialsHandle credentials = readCredentials();

    std::cerr << "Got Here 2" << std::endl;
    coreaws::Method putMethod = coreaws::MethodUtils::PUT;
    coreaws::Scheme scheme = coreaws::SchemeUtils::HTTPS;
    std::string host = "dowdandassociates-test.s3.amazonaws.com";
    coreaws::port_t port = coreaws::Port::DEFAULT;
    std::string path = "/test x.txt";
    coreaws::ParameterMap parameters;
    std::string fragment = "";
    coreaws::HeaderMap headers;
    coreaws::InputHandle inputFile = coreaws::StreamUtils::openInputFile("input.txt");
    std::cerr << "Got Here 3" << std::endl;
    coreaws::RequestHandle putRequest(new coreaws::Request(putMethod, scheme, host, port, path, parameters, fragment, headers, inputFile));
   
    std::cerr << "Got Here 4" << std::endl;
    coreaws::ResponseHandle putResponse = coreaws::Connection::execute(signer, credentials, putRequest);
    std::cerr << "Got Here 5" << std::endl;

    CURLcode rc = putResponse->returnCode;
    if (rc != CURLE_OK)
    {
        std::cerr << "Error: " << putResponse->errorMessage << " [" << curl_easy_strerror(rc) << "] (" << rc << ')' << std::endl;
    }

    coreaws::OutputHandle putOutputStream = putResponse->output;
    std::tr1::shared_ptr<std::stringstream> putOutput = std::tr1::static_pointer_cast<std::stringstream>(putOutputStream);
    std::cout << putOutput->str() << std::endl;

    std::cerr << "Got Here 6" << std::endl;
    coreaws::Method getMethod = coreaws::MethodUtils::GET;
    
    std::cerr << "Got Here 7" << std::endl;
    coreaws::InputHandle emptyInput = coreaws::StreamUtils::emptyInputStream();
    std::cerr << "Got Here 8" << std::endl;
//    coreaws::OutputHandle outputFile = coreaws::StreamUtils::emptyOutputStream();
    coreaws::OutputHandle outputFile = coreaws::StreamUtils::openOutputFile("output.txt");

    std::cerr << "Got Here 9" << std::endl;
    coreaws::RequestHandle getRequest(new coreaws::Request(getMethod, scheme, host, port, path, parameters, fragment, headers, emptyInput));
    
    std::cerr << "Got Here 10" << std::endl;
    coreaws::ResponseHandle getResponse = coreaws::Connection::execute(signer, credentials, getRequest, outputFile);

    std::cerr << "Got Here 11" << std::endl;
    rc = getResponse->returnCode;
    if (rc != CURLE_OK)
    {
        std::cerr << "Error: " << getResponse->errorMessage << " [" << curl_easy_strerror(rc) << "] (" << rc << ')' << std::endl;
    }


    std::cerr << "Got Here 12" << std::endl;
/*    
    coreaws::OutputHandle getOutputStream = getResponse->output;
    std::tr1::shared_ptr<std::stringstream> getOutput = std::tr1::static_pointer_cast<std::stringstream>(getOutputStream);
    std::cout << getOutput->str() << std::endl;
*/
    std::cerr << "Got Here 13" << std::endl;
    coreaws::Connection::cleanup();
    return 0;
    }
    catch (std::exception e)
    {
        std::cerr << "Exception: " << e.what() << std::endl;
        return 1;
    }
    catch (...)
    {
        std::cerr << "Unknown exception" << std::endl;
        return 1;
    }

}

