/*
 *
 * libcoreaws2/src/coreaws/AWS2Signer.hpp
 *
 *-------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS__AWS2_SIGNER
#define COREAWS__AWS2_SIGNER

#include "Signer.hpp"

#include <ctime>
#include <string>

#include "ParameterMap.hpp"
#include "ParameterPair.hpp"

namespace coreaws
{

class AWS2Signer : public Signer
{
public:
    static RequestHandle sign(AWSCredentialsHandle credentials, RequestHandle request, time_t time);

    AWS2Signer();
    virtual ~AWS2Signer();
    virtual RequestHandle operator ()(AWSCredentialsHandle credentials, RequestHandle request, time_t time) const;

private:
    static std::string getStringToSign(
            const std::string& method,
            const std::string& server,
            const std::string& path,
            const ParameterMap& parameters);
};

}

#endif // not COREAWS__AWS2_SIGNER

