/*
 *
 * libcoreaws2/src/coreaws/AWS3HttpsSigner.cpp
 *
 *-------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 *
 */

#include "AWS3HttpsSigner.hpp"

#include <algorithm>
#include <sstream>

#include "DateTimeUtils.hpp"
#include "HeaderUtils.hpp"
#include "StringUtils.hpp"

namespace coreaws
{

RequestHandle AWS3HttpsSigner::sign(AWSCredentialsHandle credentials,
                                    RequestHandle request,
                                    time_t time,
                                    const std::string& algorithm)
{
    HeaderMap headers(HeaderUtils::filterXAmznAuthorization(request->headers));
    HeaderMap lowerHeaders(HeaderUtils::lowerKey(headers));

    if (lowerHeaders.find("date") == lowerHeaders.end() && lowerHeaders.find("x-amz-date") == lowerHeaders.end())
    {
        std::string key = "Date";
        std::string value = DateTimeUtils::rfc822(time);
        headers.insert(HeaderPair(key, value));
        lowerHeaders.insert(HeaderPair(StringUtils::toLowerCase(key), value));
    }

    std::string stringToSign = AWS3HttpsSigner::getStringToSign(lowerHeaders);
    std::string signature = Signer::sign(algorithm, credentials->getAWSSecretKey(), stringToSign);
    std::stringstream xAmznAuthorization;
    xAmznAuthorization << "AWS3-HTTPS AWSAccessKeyId=" << credentials->getAWSAccessKeyId() << ",Algorithm=" << algorithm << ",Signature=" << signature;
    
    headers.insert(HeaderPair("X-Amzn-Authorization", xAmznAuthorization.str()));

    RequestHandle signedRequest(new Request(
            request->method,
            request->scheme,
            request->host,
            request->port,
            request->path,
            request->parameters,
            request->fragment,
            headers,
            request->inputStream));

    return signedRequest;
}

AWS3HttpsSigner::AWS3HttpsSigner() :
        algorithm("HmacSHA256")
{
}

AWS3HttpsSigner::AWS3HttpsSigner(const std::string& algorithm) :
        algorithm(algorithm)
{
}

AWS3HttpsSigner::~AWS3HttpsSigner()
{
}

RequestHandle AWS3HttpsSigner::operator ()(AWSCredentialsHandle credentials,
                                           RequestHandle request,
                                           time_t time) const
{
    return AWS3HttpsSigner::sign(credentials, request, time, this->algorithm);
}

std::string AWS3HttpsSigner::getStringToSign(const HeaderMap& lowerHeaders)
{
    HeaderMap::const_iterator ptr;

    ptr = lowerHeaders.find("x-amz-date");
    if (ptr != lowerHeaders.end())
    {
        return ptr->second;
    }

    ptr = lowerHeaders.find("date");
    if (ptr != lowerHeaders.end())
    {
        return ptr->second;
    }

    return std::string();
}

}

