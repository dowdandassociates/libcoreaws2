/*
 *
 * libcoreaws2/src/coreaws/AWS3Signer.cpp
 *
 *-------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 *
 */

#include "AWS3Signer.hpp"

#include <algorithm>
#include <numeric>
#include <sstream>
#include <stdexcept>

#include "BufferUtils.hpp"
#include "CryptoUtils.hpp"
#include "DateTimeUtils.hpp"
#include "HeaderMap.hpp"
#include "HeaderPair.hpp"
#include "HeaderUtils.hpp"
#include "ParameterUtils.hpp"
#include "PathUtils.hpp"
#include "StreamUtils.hpp"
#include "StringUtils.hpp"

namespace coreaws
{

RequestHandle AWS3Signer::sign(AWSCredentialsHandle credentials, RequestHandle request, time_t time, const std::string& algorithm)
{
    std::string digest;
    if (algorithm == "HmacSHA256")
    {
        digest = "sha256";
    }
    else if (algorithm == "HmacSHA1")
    {
        digest = "sha1";
    }
    else
    {
        std::stringstream errbuf;
        errbuf << "Unknown signing algorithm: " << algorithm;
        throw std::invalid_argument(errbuf.str());
    }

    HeaderMap headers(HeaderUtils::filterXAmznAuthorization(request->headers));
    HeaderMap lowerHeaders(HeaderUtils::lowerKey(headers));

    bool hasSecurityToken = false;
    HeaderMap::const_iterator xAmzSecurityToken = lowerHeaders.find("x-amz-security-token");
    if (xAmzSecurityToken != lowerHeaders.end())
    {
        if (!credentials->hasSessionToken() || credentials->getSessionToken() != xAmzSecurityToken->second)
        {
            throw std::invalid_argument("SecurityToken does not match credentials");
        }
        else
        {
            hasSecurityToken = true;
        }
    }
    if (!hasSecurityToken && credentials->hasSessionToken())
    {
        std::string key = "X-Amz-Security-Token";
        std::string value = credentials->getSessionToken();
        headers.insert(HeaderPair(key, value));
        lowerHeaders.insert(HeaderPair(StringUtils::toLowerCase(key), value));
    }

    if (lowerHeaders.find("date") == lowerHeaders.end())
    {
        std::string key = "Date";
        std::string value = DateTimeUtils::rfc822(time);
        headers.insert(HeaderPair(key, value));
        lowerHeaders.insert(HeaderPair(StringUtils::toLowerCase(key), value));
    }

    if (lowerHeaders.find("x-amz-date") == lowerHeaders.end())
    {
        std::string key = "X-Amz-Date";
        std::string value = DateTimeUtils::rfc822(time);
        headers.insert(HeaderPair(key, value));
        lowerHeaders.insert(HeaderPair(StringUtils::toLowerCase(key), value));
    }

    if (lowerHeaders.find("host") == lowerHeaders.end())
    {
        lowerHeaders.insert(HeaderPair("host", request->server()));
    }

    std::string stringToSign = AWS3Signer::getStringToSign(request->method, request->path, request->parameters, lowerHeaders, request->inputStream);
    Buffer bufferToSign = CryptoUtils::hash(digest, BufferUtils::fromString(stringToSign));
    std::string signature = Signer::sign(algorithm, credentials->getAWSSecretKey(), bufferToSign);

    std::stringstream strbuf;
    strbuf << "AWS3 AWSAccessKeyId=" << credentials->getAWSAccessKeyId()
           << ",Algorithm=" << algorithm
           << ",SignedHeaders=" << AWS3Signer::getSignedHeaders(headers)
           << ",Signature=" << signature;

    headers.insert(HeaderPair("X-Amzn-Authorization", strbuf.str()));

    RequestHandle signedRequest(new Request(
            request->method,
            request->scheme,
            request->host,
            request->port,
            request->path,
            request->parameters,
            request->fragment,
            headers,
            request->inputStream));

    return signedRequest;
}

AWS3Signer::AWS3Signer() : 
        algorithm("HmacSHA256")
{
}

AWS3Signer::AWS3Signer(const std::string& algorithm) :
        algorithm(algorithm)
{
}

AWS3Signer::~AWS3Signer()
{
}

RequestHandle AWS3Signer::operator ()(AWSCredentialsHandle credentials, RequestHandle request, time_t time) const
{
    return AWS3Signer::sign(credentials, request, time, algorithm);
}

std::string AWS3Signer::getStringToSign(const Method& method,
                                        const std::string& path,
                                        const ParameterMap& parameters,
                                        const HeaderMap& lowerHeaders,
                                        InputHandle inputStream)
{
    std::stringstream strbuf;

    strbuf << AWS3Signer::getMethodToSign(method);
    strbuf << AWS3Signer::getPathToSign(path);
    strbuf << AWS3Signer::getQueryStringToSign(parameters);
    strbuf << AWS3Signer::getHeadersToSign(lowerHeaders);
    strbuf << StreamUtils::getInputContents(inputStream);

    return strbuf.str();
}

std::string AWS3Signer::getMethodToSign(const Method& method)
{
    return method + std::string("\n");
}

std::string AWS3Signer::getPathToSign(const std::string& path)
{
    return PathUtils::canonicalPath(path) + std::string("\n");
}

std::string AWS3Signer::getQueryStringToSign(const ParameterMap& parameters)
{
    return ParameterUtils::queryString(parameters) + std::string("\n");
}

bool AWS3Signer::isNotInterestingHeader(const HeaderPair& header)
{
    std::string key = StringUtils::toLowerCase(header.first);

    if (key == "host")
    {
        return false;
    }

    if (StringUtils::startsWith(key, "x-amz"))
    {
        return false;
    }

    return true;
}

HeaderMap AWS3Signer::getInterestingHeaders(const HeaderMap& lowerHeaders)
{
    HeaderMap interestingHeaders;
    std::remove_copy_if(lowerHeaders.begin(),
                        lowerHeaders.end(),
                        std::inserter(interestingHeaders, interestingHeaders.end()),
                        AWS3Signer::isNotInterestingHeader);

    return interestingHeaders;
}

std::string AWS3Signer::getHeadersToSign(const HeaderMap& lowerHeaders)
{
    HeaderMap interestingHeaders(AWS3Signer::getInterestingHeaders(lowerHeaders));
    std::string headersToSign = std::accumulate(interestingHeaders.begin(), interestingHeaders.end(), std::string(), AWS3Signer::buildHeadersToSign);
    return headersToSign + std::string("\n");
}

std::string AWS3Signer::buildHeadersToSign(const std::string& acc, const HeaderPair& header)
{
    std::stringstream strbuf;

    strbuf << acc << header.first << ':' << header.second << '\n';

    return strbuf.str();
}

std::string AWS3Signer::getSignedHeaders(const HeaderMap& headers)
{
    std::string signedHeaders = std::accumulate(headers.begin(), headers.end(), std::string("Host"), AWS3Signer::buildSignedHeaders);
    return signedHeaders;
}

std::string AWS3Signer::buildSignedHeaders(const std::string& acc, const HeaderPair& header)
{
    std::stringstream strbuf;

    strbuf << acc;

    if (StringUtils::startsWith(StringUtils::toLowerCase(header.first), "x-amz"))
    {
        strbuf << ';' << header.first;
    }

    return strbuf.str();
}

}

