/*
 *
 * libcoreaws2/src/coreaws/AwsHostNameUtils.hpp
 *
 *-------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS__AWS_HOST_NAME_UTILS
#define COREAWS__AWS_HOST_NAME_UTILS

#include <string>

namespace coreaws
{

class AwsHostNameUtils
{
public:
    static std::string parseRegionName(const std::string& host);
    static std::string parseServiceName(const std::string& host);
};

}

#endif // not COREAWS__AWS_HOST_NAME_UTILS

