/*
 *
 * libcoreaws2/src/coreaws/BasicAWSCredentials.cpp
 *
 *-------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 *
 */

#include "BasicAWSCredentials.hpp"

namespace coreaws
{

BasicAWSCredentials::BasicAWSCredentials(const std::string& awsAccessKeyId, const std::string& awsSecretKey)
{
    this->awsAccessKeyId = awsAccessKeyId;
    this->awsSecretKey = awsSecretKey;
}

BasicAWSCredentials::~BasicAWSCredentials()
{
}

std::string BasicAWSCredentials::getAWSAccessKeyId() const
{
    return this->awsAccessKeyId;
}

std::string BasicAWSCredentials::getAWSSecretKey() const
{
    return this->awsSecretKey;
}

std::string BasicAWSCredentials::getSessionToken() const
{
    return "";
}

bool BasicAWSCredentials::hasSessionToken() const
{
    return false;
}

}

