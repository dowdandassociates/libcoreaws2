/*
 *
 * libcoreaws2/src/coreaws/BasicSessionCredentials.hpp
 *
 *-------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS__BASIC_SESSION_CREDENTIALS
#define COREAWS__BASIC_SESSION_CREDENTIALS

#include "AWSCredentials.hpp"

namespace coreaws
{

class BasicSessionCredentials : public AWSCredentials
{
public:
    BasicSessionCredentials(const std::string& awsAccessKeyId, const std::string& awsSecretKey, const std::string& sessionToken);

    virtual ~BasicSessionCredentials();

    virtual std::string getAWSAccessKeyId() const;
    virtual std::string getAWSSecretKey() const;
    virtual std::string getSessionToken() const;
    virtual bool hasSessionToken() const;
    
private:
    std::string awsAccessKeyId;
    std::string awsSecretKey;
    std::string sessionToken;
};

}

#endif // not COREAWS__BASIC_SESSION_CREDENTIALS

