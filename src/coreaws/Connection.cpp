/*
 *
 * libcoreaws2/src/coreaws/Connection.cpp
 *
 *-------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 *
 */

#include "Connection.hpp"

#include <cstddef>
#include <cstring>
#include <numeric>
#include <sstream>
#include <stdexcept>

#include "DateTimeUtils.hpp"
#include "MethodUtils.hpp"
#include "Response.hpp"
#include "StreamUtils.hpp"

#include <iostream>

namespace coreaws
{

void Connection::init()
{
    curl_global_init(CURL_GLOBAL_SSL);
}

void Connection::cleanup()
{
    curl_global_cleanup();
}

ResponseHandle Connection::execute(SignerHandle signer, AWSCredentialsHandle credentials, RequestHandle request)
{
    return Connection::execute(signer, credentials, request, DateTimeUtils::now());
}

ResponseHandle Connection::execute(SignerHandle signer, AWSCredentialsHandle credentials, RequestHandle request, OutputHandle output)
{
    return Connection::execute(signer, credentials, request, DateTimeUtils::now(), output);
}

ResponseHandle Connection::execute(SignerHandle signer, AWSCredentialsHandle credentials, RequestHandle request, OutputHandle output, OutputHandle header)
{
    return Connection::execute(signer, credentials, request, DateTimeUtils::now(), output, header);
}

ResponseHandle Connection::execute(SignerHandle signer, AWSCredentialsHandle credentials, RequestHandle request, std::time_t time)
{
    RequestHandle signedRequest = (*signer)(credentials, request, time);
    return Connection::execute(signedRequest);
}

ResponseHandle Connection::execute(SignerHandle signer, AWSCredentialsHandle credentials, RequestHandle request, std::time_t time, OutputHandle output)
{
    RequestHandle signedRequest = (*signer)(credentials, request, time);
    return Connection::execute(signedRequest, output);
}

ResponseHandle Connection::execute(SignerHandle signer, AWSCredentialsHandle credentials, RequestHandle request, std::time_t time, OutputHandle output, OutputHandle header)
{
    RequestHandle signedRequest = (*signer)(credentials, request, time);
    return Connection::execute(signedRequest, output, header);
}

ResponseHandle Connection::execute(RequestHandle request)
{
    OutputHandle output = StreamUtils::emptyOutputStream();
    return Connection::execute(request, output);
}

ResponseHandle Connection::execute(RequestHandle request, OutputHandle output)
{
    OutputHandle header = StreamUtils::emptyOutputStream();
    return Connection::execute(request, output, header);
}

ResponseHandle Connection::execute(RequestHandle request, OutputHandle output, OutputHandle header)
{
//    std::cerr << request->url() << std::endl;
//    std::cerr << request->queryString() << std::endl;

//    std::cerr << "Connection::execute: init" << std::endl;
    CURL* curl = Connection::initCurl();

//    Connection::setVerbose(curl, true);
    Connection::setVerbose(curl, false);

    Connection::setURL(curl, request->url());
    Method method = request->method;
    Connection::setMethod(curl, method);
    std::string queryString = request->queryString();
    if (method == MethodUtils::POST)
    {
        std::size_t length = StreamUtils::getInputSize(request->inputStream);
        if (length > 0)
        {
            Connection::setInputStream(curl, request->inputStream);
            Connection::setPostFieldSize(curl, length);
        }
        else
        {
            Connection::setPostFields(curl, queryString);
            Connection::setPostFieldSize(curl, queryString.length());
        }
    }
    else if (method == MethodUtils::PUT)
    {
//        std::cerr << "Connection::execute: set input stream" << std::endl;
        Connection::setInputStream(curl, request->inputStream);
//        std::cerr << "Connection::execute: set in file size" << std::endl;
        Connection::setInFileSize(curl, StreamUtils::getInputSize(request->inputStream));
    }

//    std::cerr << "Connection::execute: set headers" << std::endl;
    curl_slist* slist = Connection::setHttpHeaders(curl, request->headers);
    Connection::setHeaderStream(curl, header);
    Connection::setOutputStream(curl, output);
    Connection::setFollowLocation(curl, true);
    char* errorBuffer = Connection::setErrorBuffer(curl);
//    std::cerr << "Connection::execute: perform" << std::endl;
    CURLcode rc = curl_easy_perform(curl);
//    std::cerr << "Connection::execute: performed" << std::endl;

//    std::string errorMessage = errorBuffer;
//    delete[] errorBuffer;
//    curl_slist_free_all(slist);

//    ResponseHandle response(new Response(curl, rc, errorMessage, output, header));
    ResponseHandle response(new Response(curl, rc, errorBuffer, output, header, slist));

    return response;
}

CURL* Connection::initCurl()
{
    CURL* curl = curl_easy_init();
    if (curl == NULL)
    {
        throw std::runtime_error("Cannot allocate CURL handle");
    }

    return curl;
}

char* Connection::setErrorBuffer(CURL* curl)
{
    char* errorMessage = new char[CURL_ERROR_SIZE + 1];
    memset(errorMessage, 0, CURL_ERROR_SIZE + 1);
    CURLcode rc = curl_easy_setopt(curl, CURLOPT_ERRORBUFFER, errorMessage);
    if (rc != CURLE_OK)
    {
        curl_easy_cleanup(curl);
        delete [] errorMessage;
        throw std::runtime_error(Connection::error("Error setting error buffer", rc));
    }

    return errorMessage;
}

void Connection::setFollowLocation(CURL* curl, bool follow)
{
    long value = (follow) ? 1 : 0;
    CURLcode rc = curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, value);
    if (rc != CURLE_OK)
    {
        curl_easy_cleanup(curl);
        throw std::runtime_error(Connection::error("Error setting follow location", rc));
    }
}

curl_slist* Connection::setHttpHeaders(CURL* curl, HeaderMap headers)
{
    curl_slist* slist = std::accumulate(headers.begin(), headers.end(), reinterpret_cast<curl_slist*>(NULL), Connection::setHttpHeader);

    CURLcode rc = curl_easy_setopt(curl, CURLOPT_HTTPHEADER, slist);
    if (rc != CURLE_OK)
    {
        curl_easy_cleanup(curl);
        throw std::runtime_error(Connection::error("Error setting HTTP headers", rc));
    }

    return slist;
}

curl_slist* Connection::setHttpHeader(curl_slist* acc, const HeaderPair& pair)
{
    std::string header = pair.first + std::string(": ") + pair.second;
    acc = curl_slist_append(acc, header.c_str());
    return acc;
}

void Connection::setInFileSize(CURL* curl, curl_off_t size)
{
    CURLcode rc = curl_easy_setopt(curl, CURLOPT_INFILESIZE_LARGE, size);
    if (rc != CURLE_OK)
    {
        curl_easy_cleanup(curl);
        throw std::runtime_error(Connection::error("Error setting infile size", rc));
    }
}

void Connection::setInputStream(CURL* curl, InputHandle inputStream)
{
    CURLcode rc = curl_easy_setopt(curl, CURLOPT_READFUNCTION, Connection::read);
    if (rc != CURLE_OK)
    {
        curl_easy_cleanup(curl);
        throw std::runtime_error(Connection::error("Error setting read function", rc));
    }

    rc = curl_easy_setopt(curl, CURLOPT_READDATA, reinterpret_cast<void*>(inputStream.get()));
    if (rc != CURLE_OK)
    {
        curl_easy_cleanup(curl);
        throw std::runtime_error(Connection::error("Error setting read stream", rc));
    }
}

void Connection::setOutputStream(CURL* curl, OutputHandle outputStream)
{
    CURLcode rc = curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, Connection::write);
    if (rc != CURLE_OK)
    {
        curl_easy_cleanup(curl);
        throw std::runtime_error(Connection::error("Error setting write function", rc));
    }

    rc = curl_easy_setopt(curl, CURLOPT_WRITEDATA, reinterpret_cast<void*>(outputStream.get()));
    if (rc != CURLE_OK)
    {
        curl_easy_cleanup(curl);
        throw std::runtime_error(Connection::error("Error setting write stream", rc));
    }
}

void Connection::setHeaderStream(CURL* curl, OutputHandle headerStream)
{
    CURLcode rc = curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, Connection::write);
    if (rc != CURLE_OK)
    {
        curl_easy_cleanup(curl);
        throw std::runtime_error(Connection::error("Error setting header function", rc));
    }

    rc = curl_easy_setopt(curl, CURLOPT_WRITEHEADER, reinterpret_cast<void*>(headerStream.get()));
    if (rc != CURLE_OK)
    {
        curl_easy_cleanup(curl);
        throw std::runtime_error(Connection::error("Error setting header stream", rc));
    }
}

void Connection::setMethod(CURL* curl, const Method& method)
{
    CURLcode rc;

    if (method == MethodUtils::GET)
    {
        rc = curl_easy_setopt(curl, CURLOPT_HTTPGET, 1);
    }
    else if (method == MethodUtils::POST)
    {
        rc = curl_easy_setopt(curl, CURLOPT_POST, 1);
    }
    else if (method == MethodUtils::PUT)
    {
        rc = curl_easy_setopt(curl, CURLOPT_UPLOAD, 1);
    }
    else if (method == MethodUtils::DELETE)
    {
        rc = curl_easy_setopt(curl, CURLOPT_CUSTOMREQUEST, "DELETE");
    }
    else if (method == MethodUtils::HEAD)
    {
        rc = curl_easy_setopt(curl, CURLOPT_NOBODY, 1);
    }
    else
    {
        std::stringstream errbuf;
        errbuf << "Unknown HTTP method: " << method;
        curl_easy_cleanup(curl);
        throw std::invalid_argument(errbuf.str());
    }

    if (rc != CURLE_OK)
    {
        std::stringstream errbuf;
        errbuf << "Error setting method " << method;
        curl_easy_cleanup(curl);
        throw std::runtime_error(Connection::error(errbuf.str(), rc));
    }
}

void Connection::setPostFields(CURL* curl, const std::string& postField)
{
    CURLcode rc = curl_easy_setopt(curl, CURLOPT_POSTFIELDS, postField.c_str());
    if (rc != CURLE_OK)
    {
        curl_easy_cleanup(curl);
        throw std::runtime_error(Connection::error("Error setting post fields", rc));
    }
}

void Connection::setPostFieldSize(CURL* curl, curl_off_t size)
{
    CURLcode rc = curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE_LARGE, size);
    if (rc != CURLE_OK)
    {
        curl_easy_cleanup(curl);
        throw std::runtime_error(Connection::error("Error setting post field size", rc));
    }
}

void Connection::setURL(CURL* curl, const std::string& url)
{
    CURLcode rc = curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
    if (rc != CURLE_OK)
    {
        curl_easy_cleanup(curl);
        throw std::runtime_error(Connection::error("Error setting URL", rc));
    }
}

void Connection::setVerbose(CURL* curl, bool verbose)
{
    long value = (verbose) ? 1 : 0;
    CURLcode rc = curl_easy_setopt(curl, CURLOPT_VERBOSE, value);
    if (rc != CURLE_OK)
    {
        curl_easy_cleanup(curl);
        throw std::runtime_error(Connection::error("Error setting verbose", rc));
    }
}

std::string Connection::error(CURLcode rc)
{
    std::stringstream strbuf;

    strbuf << curl_easy_strerror(rc) << " (" << rc << ')';
    return strbuf.str();
}

std::string Connection::error(const std::string& message, CURLcode rc)
{
    std::stringstream strbuf;

    strbuf << message << ": " << Connection::error(rc);
    return strbuf.str();
}

size_t Connection::read(void* ptr, size_t size, size_t nmemb, void* data)
{

    std::istream* input;
    char buffer[1];
    int cnt;
    int len;

    if (data == NULL)
    {
        throw std::invalid_argument("NULL pointer passed to input callback");
    }

    input = reinterpret_cast<std::istream*>(data);

    for (cnt = 0, len = size * nmemb; cnt < len; ++cnt)
    {
        input->read(buffer, 1);
        if (input->eof() || input->fail())
        {
            break;
        }
        memcpy(((char*)ptr) + cnt, buffer, 1);
    }

    return cnt;
}

size_t Connection::write(void* ptr, size_t size, size_t nmemb, void* data)
{
    std::ostream* output;
    char* buffer;
    int i;
    int len;
    int pos;

    if (data == NULL)
    {
        throw std::invalid_argument("NULL pointer passed to output callback");
    }

    output = reinterpret_cast<std::ostream*>(data);
    buffer = reinterpret_cast<char*>(ptr);

    for (i = 0, pos = 0; i < nmemb; ++i, pos += size)
    {
        output->write(buffer + pos, size);
        if (output->bad())
        {
            break;
        }
    }

    return pos;
}

}

