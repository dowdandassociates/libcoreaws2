/*
 *
 * libcoreaws2/src/coreaws/Connection.hpp
 *
 *-------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS__CONNECTION
#define COREAWS__CONNECTION

#include <ctime>
#include <istream>
#include <ostream>
#include <string>
#include <tr1/memory>

#include <curl/curl.h>

#include "AWSCredentialsHandle.hpp"
#include "HeaderPair.hpp"
#include "RequestHandle.hpp"
#include "ResponseHandle.hpp"
#include "SignerHandle.hpp"

namespace coreaws
{

class Connection
{
public:
    static void init();
    static void cleanup();

    static ResponseHandle execute(SignerHandle signer, AWSCredentialsHandle credentials, RequestHandle request);
    static ResponseHandle execute(SignerHandle signer, AWSCredentialsHandle credentials, RequestHandle request, OutputHandle output);
    static ResponseHandle execute(SignerHandle signer, AWSCredentialsHandle credentials, RequestHandle request, OutputHandle output, OutputHandle header);

    static ResponseHandle execute(SignerHandle signer, AWSCredentialsHandle credentials, RequestHandle request, std::time_t time);
    static ResponseHandle execute(SignerHandle signer, AWSCredentialsHandle credentials, RequestHandle request, std::time_t time, OutputHandle output);
    static ResponseHandle execute(SignerHandle signer, AWSCredentialsHandle credentials, RequestHandle request, std::time_t time, OutputHandle output, OutputHandle header);

    static ResponseHandle execute(RequestHandle request);
    static ResponseHandle execute(RequestHandle request, OutputHandle output);
    static ResponseHandle execute(RequestHandle request, OutputHandle output, OutputHandle header);

    static size_t read(void* ptr, size_t size, size_t nmemb, void* data);
    static size_t write(void* ptr, size_t size, size_t nmemb, void* data);

private:
    static CURL* initCurl();
    static char* setErrorBuffer(CURL* curl);
    static void setFollowLocation(CURL* curl, bool follow);
    static curl_slist* setHttpHeaders(CURL* curl, HeaderMap headers);
    static curl_slist* setHttpHeader(curl_slist* acc, const HeaderPair& pair);
    static void setInFileSize(CURL* curl, curl_off_t size);
    static void setInputStream(CURL* curl, InputHandle inputStream);
    static void setOutputStream(CURL* curl, OutputHandle outputStream);
    static void setHeaderStream(CURL* curl, OutputHandle headerStream);
    static void setMethod(CURL* curl, const Method& method);
    static void setPostFields(CURL* curl, const std::string& postField);
    static void setPostFieldSize(CURL* curl, curl_off_t size);
    static void setURL(CURL* curl, const std::string& url);
    static void setVerbose(CURL* curl, bool verbose);
    static std::string error(CURLcode rc);
    static std::string error(const std::string& message, CURLcode rc);
};

}

#endif // not COREAWS__CONNECTION

