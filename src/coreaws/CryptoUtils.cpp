/*
 *
 * libcoreaws2/src/coreaws/CryptoUtils.cpp
 *
 *-------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 *
 */

#include "CryptoUtils.hpp"

#include <cstring>
#include <numeric>
#include <sstream>
#include <stdexcept>

#include <openssl/bio.h>
#include <openssl/buffer.h>
#include <openssl/evp.h>
#include <openssl/hmac.h>
#include <openssl/sha.h>

#include "BufferUtils.hpp"

namespace coreaws
{

Buffer CryptoUtils::hmac(const std::string& digest, const Buffer& key, const Buffer& data)
{
    OpenSSL_add_all_digests();

    const EVP_MD* md = EVP_get_digestbyname(digest.c_str());
    if (!md)
    {
        std::stringstream errbuf;
        errbuf << "Unknown message digest: \"" << digest << '"';
        throw std::domain_error(errbuf.str());
    }

    HMAC_CTX ctx;
    HMAC_CTX_init(&ctx);
    HMAC_Init(&ctx, key.data(), key.size(), md);

    unsigned char* buf = new unsigned char[EVP_MAX_MD_SIZE];
    unsigned int len;
    HMAC(md, key.data(), key.size(), data.data(), data.size(), buf, &len);

    Buffer buffer = BufferUtils::fromArray(buf, len);

    delete [] buf;
    HMAC_CTX_cleanup(&ctx);

    return buffer;
}

Buffer CryptoUtils::hash(const std::string& digest, const Buffer& data)
{
    OpenSSL_add_all_digests();

    const EVP_MD* md = EVP_get_digestbyname(digest.c_str());
    if (!md)
    {
        std::stringstream errbuf;
        errbuf << "Unknown message digest: \"" << digest << '"';
        throw std::domain_error(errbuf.str());
    }

    EVP_MD_CTX ctx;
    unsigned char* buf = new unsigned char[EVP_MAX_MD_SIZE];
    unsigned int len;

    EVP_MD_CTX_init(&ctx);
    EVP_DigestInit_ex(&ctx, md, NULL);
    EVP_DigestUpdate(&ctx, data.data(), data.size());
    EVP_DigestFinal_ex(&ctx, buf, &len);

    std::vector<unsigned char> buffer = BufferUtils::fromArray(buf, len);

    delete [] buf;
    EVP_MD_CTX_cleanup(&ctx);

    return buffer;
}

std::string CryptoUtils::encodeBase64(const Buffer& data)
{
    BIO* b64 = BIO_new(BIO_f_base64());
    BIO* bio = BIO_new(BIO_s_mem());
    BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);
    bio = BIO_push(b64, bio);
    BIO_write(bio, data.data(), data.size());
    BIO_flush(bio);

    char* tmp;
    long len = BIO_get_mem_data(bio, &tmp);
    char* encoded = new char[len + 1];
    memset(encoded, 0, len + 1);
    memcpy(encoded, tmp, len);

    std::string base64 = encoded;

    delete [] encoded;
    BIO_free_all(bio);

    return base64;
}

Buffer CryptoUtils::decodeBase64(const std::string& base64)
{
    int len = base64.size();
    unsigned char* buf = new unsigned char[len];
    memset(buf, 0, len);
    BIO* b64 = BIO_new(BIO_f_base64());
    BIO* bmem = BIO_new_mem_buf((void*)base64.data(), len);
    bmem = BIO_push(b64, bmem);
    BIO_read(bmem, buf, len);
    Buffer buffer = BufferUtils::fromArray(buf, len);
    BIO_free_all(bmem);
    delete [] buf;

    return buffer;
}

std::string CryptoUtils::toHex(const Buffer& data)
{
    return std::accumulate(data.begin(), data.end(), std::string(), CryptoUtils::buildHex);
}

std::string CryptoUtils::buildHex(const std::string& acc, unsigned char c)
{
    char buf[3];
    memset(buf, 0, 3);

    sprintf(buf, "%02x", c);

    return acc + std::string(buf);
}

}

