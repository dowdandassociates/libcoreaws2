/*
 *
 * libcoreaws2/src/coreaws/HeaderUtils.cpp
 *
 *-------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 *
 */

#include "HeaderUtils.hpp"

#include <algorithm>

#include "StringUtils.hpp"

namespace coreaws
{

HeaderMap HeaderUtils::lowerKey(const HeaderMap& headers)
{
    HeaderMap lowerHeaders;
    std::transform(headers.begin(),
                   headers.end(),
                   std::inserter(lowerHeaders, lowerHeaders.end()),
                   HeaderUtils::lowerCaseKey);
    return lowerHeaders;
}

HeaderMap HeaderUtils::filterAuthorization(const HeaderMap& headers)
{
    HeaderMap newHeaders;
    std::remove_copy_if(headers.begin(),
                        headers.end(),
                        std::inserter(newHeaders, newHeaders.end()),
                        HeaderUtils::isAuthorization);
    return newHeaders;
}

HeaderMap HeaderUtils::filterXAmznAuthorization(const HeaderMap& headers)
{
    HeaderMap newHeaders;
    std::remove_copy_if(headers.begin(),
                        headers.end(),
                        std::inserter(newHeaders, newHeaders.end()),
                        HeaderUtils::isXAmznAuthorization);
    return newHeaders;
}

bool HeaderUtils::isXAmznAuthorization(const HeaderPair& header)
{
    return (StringUtils::toLowerCase(header.first) == "x-amzn-authorization");
}

bool HeaderUtils::isAuthorization(const HeaderPair& header)
{
    return (StringUtils::toLowerCase(header.first) == "authorization");
}

HeaderPair HeaderUtils::lowerCaseKey(const HeaderPair& header)
{
    return HeaderPair(StringUtils::toLowerCase(header.first), header.second);
}

}

