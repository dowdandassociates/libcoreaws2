/*
 *
 * libcoreaws2/src/coreaws/InputHandle.hpp
 *
 *-------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS__INPUT_HANDLE
#define COREAWS__INPUT_HANDLE

#include <istream>
#include <tr1/memory>

namespace coreaws
{

typedef std::tr1::shared_ptr<std::istream> InputHandle;

}

#endif // not COREAWS__INPUT_HANDLE

