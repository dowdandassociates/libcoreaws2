/*
 *
 * libcoreaws2/src/coreaws/MethodUtils.hpp
 *
 *-------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS__METHOD_UTILS
#define COREAWS__METHOD_UTILS

namespace coreaws
{

class MethodUtils
{
public:
    static const char* DELETE;
    static const char* GET;
    static const char* HEAD;
    static const char* POST;
    static const char* PUT;
};

}

#endif // not COREAWS__METHOD_UTILS

