/*
 *
 * libcoreaws2/src/coreaws/ParameterUtils.cpp
 *
 *-------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 *
 */

#include "ParameterUtils.hpp"

#include <algorithm>
#include <iterator>
#include <cstdio>
#include <cstring>
#include <numeric>
#include <sstream>

#include "UrlUtils.hpp"

namespace coreaws
{

std::string ParameterUtils::queryString(const ParameterMap& parameters)
{
    return ParameterUtils::checkQueryString(std::accumulate(parameters.begin(), parameters.end(), std::string(), ParameterUtils::buildQueryString));
}

std::string ParameterUtils::queryStringNoEmptyEquals(const ParameterMap& parameters)
{
    return ParameterUtils::checkQueryString(std::accumulate(parameters.begin(), parameters.end(), std::string(), ParameterUtils::buildQueryStringNoEmptyEquals));
}

std::string ParameterUtils::buildQueryString(const std::string& acc, const ParameterPair& parameter)
{
    return acc + "&" + UrlUtils::escape(parameter.first) + "=" + UrlUtils::escape(parameter.second);
}

std::string ParameterUtils::buildQueryStringNoEmptyEquals(const std::string& acc, const ParameterPair& parameter)
{
    std::string buf = buildQueryString(acc, parameter);
    std::size_t last = buf.length() - 1;
    if (buf[last] == '=')
    {
        return buf.substr(0, last);
    }
    else
    {
        return buf;
    }
}

std::string ParameterUtils::checkQueryString(const std::string& queryString)
{
    if (!queryString.empty())
    {
        return queryString.substr(1);
    }
    else
    {
        return queryString;
    }
}

bool ParameterUtils::isSignature(const ParameterPair& parameter)
{
    return (parameter.first == "Signature");
}

ParameterMap ParameterUtils::filterSignature(const ParameterMap& parameters)
{
    ParameterMap newParameters;
    std::remove_copy_if(parameters.begin(),
                        parameters.end(),
                        std::inserter(newParameters, newParameters.end()),
                        ParameterUtils::isSignature);

    return newParameters;
}

}

