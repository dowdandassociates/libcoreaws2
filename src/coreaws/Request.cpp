/*
 *
 * libcoreaws2/src/coreaws/Request.cpp
 *
 *-------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 *
 */

#include "Request.hpp"

#include <cstdio>
#include <cstring>
#include <fstream>
#include <numeric>
#include <sstream>

#include "MethodUtils.hpp"
#include "ParameterUtils.hpp"
#include "PathUtils.hpp"
#include "Port.hpp"
#include "SchemeUtils.hpp"

namespace coreaws
{

Request::Request(const Method& method,
                 const Scheme& scheme,
                 const std::string& host,
                 port_t port,
                 const std::string& path,
                 const ParameterMap& parameters,
                 const std::string& fragment,
                 const HeaderMap& headers,
                 std::tr1::shared_ptr<std::istream> inputStream) :
    method(method),
    scheme(scheme),
    host(host),
    port(port),
    path(path),
    parameters(parameters),
    fragment(fragment),
    headers(headers),
    inputStream(inputStream)
{
}

Request::~Request()
{
}

std::string Request::queryString() const
{
    return ParameterUtils::queryString(this->parameters);
}

std::string Request::server() const
{
    Scheme scheme = this->scheme;
    std::string host = this->host;
    port_t port = this->port;

    if (port == Port::DEFAULT ||
            (port == Port::HTTP && scheme == SchemeUtils::HTTP) ||
            (port == Port::HTTPS && scheme == SchemeUtils::HTTPS))
    {
        return host;
    }
    else
    {

        std::stringstream strbuf;
        strbuf << host << ':' << port;

        return strbuf.str();
    }
}

std::string Request::url() const
{
    std::stringstream strbuf;

    strbuf << this->scheme;
    strbuf << "://";
    strbuf << this->server();
    strbuf << PathUtils::canonicalPath(this->path);

    std::string queryString = this->queryString();
    if (!queryString.empty() && this->method != MethodUtils::POST)
    {
        strbuf << '?' << queryString;
    }

    if (!this->fragment.empty())
    {
        strbuf << '#' << this->fragment;
    }

    return strbuf.str();
}

}

