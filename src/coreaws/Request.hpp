/*
 *
 * libcoreaws2/src/coreaws/Request.hpp
 *
 *-------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS__REQUEST
#define COREAWS__REQUEST

#include <cstddef>
#include <istream>
#include <map>
#include <string>
#include <tr1/memory>

#include "HeaderMap.hpp"
#include "InputHandle.hpp"
#include "Method.hpp"
#include "ParameterMap.hpp"
#include "ParameterPair.hpp"
#include "Port.hpp"
#include "Scheme.hpp"

namespace coreaws
{

class Request
{
public:
    Request(
        const Method& method,
        const Scheme& scheme,
        const std::string& host,
        port_t port,
        const std::string& path,
        const ParameterMap& parameters,
        const std::string& fragment,
        const HeaderMap& headers,
        InputHandle inputStream);

    virtual ~Request();
    virtual std::string queryString() const;
    virtual std::string server() const;
    virtual std::string url() const;

    const Method method;
    const Scheme scheme;
    const std::string host;
    const port_t port;
    const std::string path;
    const ParameterMap parameters;
    const std::string fragment;
    const HeaderMap headers;
    const InputHandle inputStream;
};

}

#endif // not COREAWS__REQUEST

