/*
 *
 * libcoreaws2/src/coreaws/Response.hpp
 *
 *-------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS__RESPONSE
#define COREAWS__RESPONSE

#include <string>

#include <curl/curl.h>

#include "OutputHandle.hpp"

namespace coreaws
{

class Response
{
public:
/*
    Response(CURL* curl,
             CURLcode returnCode,
             const std::string& errorMessage,
             OutputHandle output,
             OutputHandle header);
*/

    Response(CURL* curl,
             CURLcode returnCode,
             char* errorMessage,
             OutputHandle output,
             OutputHandle header,
             curl_slist* headerList);

    virtual ~Response();
 
    virtual long getResponseCode() const;

    virtual CURLcode getInfo(CURLINFO info, long* data) const;
    virtual CURLcode getInfo(CURLINFO info, char** data) const;
    virtual CURLcode getInfo(CURLINFO info, curl_slist** data) const;
    virtual CURLcode getInfo(CURLINFO info, double* data) const;

    const CURLcode returnCode;
    const std::string errorMessage;
    const OutputHandle output;
    const OutputHandle header;

private:
    CURL* curl;
    char* errorBuffer;
    curl_slist* headerList;
};

}

#endif // not COREAWS__RESPONSE

