/*
 *
 * libcoreaws2/src/coreaws/S3QueryStringSigner.cpp
 *
 *-------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 *
 */

#include "S3QueryStringSigner.hpp"

#include <algorithm>
#include <numeric>
#include <sstream>
#include <stdexcept>

#include "HeaderUtils.hpp"
#include "ParameterUtils.hpp"

#include <iostream>

namespace coreaws
{

RequestHandle S3QueryStringSigner::sign(AWSCredentialsHandle credentials,
                                        RequestHandle request,
                                        std::time_t expires,
                                        const std::string& bucket,
                                        const std::string& key)
{
    return S3QueryStringSigner::sign(credentials, request, expires, S3Signer::generateResourcePath(bucket, key));
}

RequestHandle S3QueryStringSigner::sign(AWSCredentialsHandle credentials,
                                        RequestHandle request,
                                        std::time_t expires,
                                        const std::string& resourcePath)
{
    if (credentials->hasSessionToken())
    {
        throw std::invalid_argument("Cannot sign S3 Query String with Session Credentials");
    }

    HeaderMap headers(HeaderUtils::filterAuthorization(request->headers));
    HeaderMap lowerHeaders(HeaderUtils::lowerKey(headers));
    ParameterMap parameters(ParameterUtils::filterSignature(request->parameters));

    ParameterMap::const_iterator awsAccessKeyIdPtr = parameters.find("AWSAccessKeyId");
    std::string accessKey = credentials->getAWSAccessKeyId();
    if (awsAccessKeyIdPtr != parameters.end())
    {
        if (awsAccessKeyIdPtr->second != accessKey)
        {
            throw std::invalid_argument("Access Key ID does not match parameter");
        }
    }
    else
    {
        parameters.insert(ParameterPair("AWSAccessKeyId", accessKey));
    }

    ParameterMap::const_iterator expiresPtr = parameters.find("Expires");
    std::string expiresStr;
    if (expiresPtr != parameters.end())
    {
        expiresStr = expiresPtr->second;
    }
    else
    {
        std::stringstream expiresStrbuf;
        expiresStrbuf << expires;
        expiresStr = expiresStrbuf.str();
        parameters.insert(ParameterPair("Expires", expiresStr));
    }

    HeaderMap::iterator date = lowerHeaders.find("date");
    if (date != lowerHeaders.end())
    {
        lowerHeaders.erase(date);
    }
    lowerHeaders.insert(HeaderPair("date", expiresStr));

    std::string stringToSign = S3Signer::getStringToSign(request->method, lowerHeaders, resourcePath, request->parameters);
    std::string signature = Signer::sign("HmacSHA1", credentials->getAWSSecretKey(), stringToSign);

//    std::cerr << "StringToSign: " << stringToSign << std::endl;

    parameters.insert(ParameterPair("Signature", signature));

    RequestHandle signedRequest(new Request(
            request->method,
            request->scheme,
            request->host,
            request->port,
            request->path,
            parameters,
            request->fragment,
            headers,
            request->inputStream));

    return signedRequest;
}

S3QueryStringSigner::S3QueryStringSigner(const std::string& bucket, const std::string& key) :
        S3Signer(bucket, key)
{
}

S3QueryStringSigner::S3QueryStringSigner(const std::string& resourcePath) :
        S3Signer(resourcePath)
{
}

S3QueryStringSigner::~S3QueryStringSigner()
{
}

RequestHandle S3QueryStringSigner::operator ()(std::tr1::shared_ptr<AWSCredentials> credentials, std::tr1::shared_ptr<Request> request, std::time_t expires) const
{
    return S3QueryStringSigner::sign(credentials, request, expires, this->resourcePath);
}

}

