/*
 *
 * libcoreaws2/src/coreaws/S3Signer.cpp
 *
 *-------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 *
 */

#include "S3Signer.hpp"

#include <algorithm>
#include <numeric>
#include <sstream>
#include <stdexcept>

#include "DateTimeUtils.hpp"
#include "HeaderUtils.hpp"
#include "ParameterUtils.hpp"
#include "PathUtils.hpp"
#include "StringUtils.hpp"

#include <iostream>

namespace coreaws
{

RequestHandle S3Signer::sign(AWSCredentialsHandle credentials,
                             RequestHandle request,
                             std::time_t time,
                             const std::string& bucket,
                             const std::string& key)
{
    return S3Signer::sign(credentials, request, time, S3Signer::generateResourcePath(bucket, key));
}

RequestHandle S3Signer::sign(AWSCredentialsHandle credentials,
                             RequestHandle request,
                             std::time_t time,
                             const std::string& resourcePath)
{
    HeaderMap headers(HeaderUtils::filterAuthorization(request->headers));
    HeaderMap lowerHeaders(HeaderUtils::lowerKey(headers));

    bool hasSecurityToken = false;
    HeaderMap::const_iterator xAmzSecurityToken = lowerHeaders.find("x-amz-security-token");
    if (xAmzSecurityToken != lowerHeaders.end())
    {
        if (!credentials->hasSessionToken() || credentials->getSessionToken() != xAmzSecurityToken->second)
        {
            throw std::invalid_argument("SecurityToken does not match credentials");
        }
        else
        {
            hasSecurityToken = true;
        }
    }

    if (!hasSecurityToken && credentials->hasSessionToken())
    {
        std::string key = "X-Amz-Security-Token";
        std::string value = credentials->getSessionToken();
        headers.insert(HeaderPair(key, value));
        lowerHeaders.insert(HeaderPair(StringUtils::toLowerCase(key), value));
    }

    if (lowerHeaders.find("date") == lowerHeaders.end() && lowerHeaders.find("x-amz-date") == lowerHeaders.end())
    {
        std::string key = "Date";
        std::string value = DateTimeUtils::rfc822(time);
        headers.insert(HeaderPair(key, value));
        lowerHeaders.insert(HeaderPair(StringUtils::toLowerCase(key), value));
    }

    std::string stringToSign = S3Signer::getStringToSign(request->method, lowerHeaders, resourcePath, request->parameters);
    std::string signature = Signer::sign("HmacSHA1", credentials->getAWSSecretKey(), stringToSign);

//    std::cerr << "StringToSign: " << stringToSign << std::endl;

    std::stringstream authorization;
    authorization << "AWS " << credentials->getAWSAccessKeyId() << ':' << signature;
    headers.insert(HeaderPair("Authorization", authorization.str()));

    RequestHandle signedRequest(new Request(
            request->method,
            request->scheme,
            request->host,
            request->port,
            request->path,
            request->parameters,
            request->fragment,
            headers,
            request->inputStream));

    return signedRequest;
}

std::string S3Signer::getStringToSign(const Method& method, const HeaderMap& lowerHeaders, const std::string& resourcePath, const ParameterMap& parameters)
{
    std::stringstream strbuf;
    strbuf << S3Signer::getMethodToSign(method)
           << S3Signer::getHeadersToSign(lowerHeaders)
           << S3Signer::getResourceToSign(resourcePath, parameters);

    return strbuf.str();
}

std::string S3Signer::getMethodToSign(const Method& method)
{
    return method + std::string("\n");
}

std::string S3Signer::getHeadersToSign(const HeaderMap& lowerHeaders)
{
    HeaderMap interestingHeaders(S3Signer::getInterestingHeaders(lowerHeaders));
    std::string headerSection = std::accumulate(interestingHeaders.begin(), interestingHeaders.end(), std::string(), S3Signer::buildHeaderSection);
//    std::cerr << "headerSection: " << headerSection << std::endl;
    return headerSection;
}

std::string S3Signer::getResourceToSign(const std::string& resourcePath, const ParameterMap& parameters)
{
    ParameterMap canonicalParameters;
    std::remove_copy_if(parameters.begin(),
                        parameters.end(),
                        std::inserter(canonicalParameters, canonicalParameters.end()),
                        S3Signer::isNotCanonicalParameter);
    
    std::string canonicalQueryString(ParameterUtils::queryStringNoEmptyEquals(canonicalParameters));

    std::stringstream strbuf;
    strbuf << PathUtils::canonicalPath(resourcePath);
    if (canonicalQueryString.length() > 0)
    {
        strbuf << '?' << canonicalQueryString;
    }

    return strbuf.str();
}

HeaderMap S3Signer::getInterestingHeaders(const HeaderMap& lowerHeaders)
{
    HeaderMap interestingHeaders;
    std::remove_copy_if(lowerHeaders.begin(),
                        lowerHeaders.end(),
                        std::inserter(interestingHeaders, interestingHeaders.end()),
                        S3Signer::isNotInterestingHeader);

    HeaderMap::const_iterator ptr = lowerHeaders.find("date");
    std::string date = (ptr != lowerHeaders.end() && lowerHeaders.find("x-amz-date") == lowerHeaders.end()) ? ptr->second : "";
    interestingHeaders.insert(HeaderPair("date", date));

    if (interestingHeaders.find("content-type") == interestingHeaders.end())
    {
        interestingHeaders.insert(HeaderPair("content-type", ""));
    }

    if (interestingHeaders.find("content-md5") == interestingHeaders.end())
    {
        interestingHeaders.insert(HeaderPair("content-md5", ""));
    }

    return interestingHeaders;
}

std::string S3Signer::buildHeaderSection(const std::string& acc, const HeaderPair& header)
{
    std::stringstream strbuf;

    strbuf << acc; 
//    std::cerr << header.first << std::endl;
    if (StringUtils::startsWith(header.first, "x-amz-"))
    {
        strbuf << header.first << ':' << header.second;
    }
    else
    {
        strbuf << header.second;
    }

    strbuf << '\n';

    return strbuf.str();
}

bool S3Signer::isNotInterestingHeader(const HeaderPair& header)
{
    if (header.first == "content-type")
    {
        return false;
    }

    if (header.first == "content-md5")
    {
        return false;
    }

    if (StringUtils::startsWith(header.first, "x-amz-"))
    {
        return false;
    }

    return true;
}

bool S3Signer::isNotCanonicalParameter(const ParameterPair& parameter)
{
    if (parameter.first == "acl")
    {
        return false;
    }

    if (parameter.first == "cors")
    {
        return false;
    }

    if (parameter.first == "delete")
    {
        return false;
    }

    if (parameter.first == "lifecycle")
    {
        return false;
    }

    if (parameter.first == "location")
    {
        return false;
    }

    if (parameter.first == "logging")
    {
        return false;
    }

    if (parameter.first == "notification")
    {
        return false;
    }

    if (parameter.first == "partNumber")
    {
        return false;
    }

    if (parameter.first == "policy")
    {
        return false;
    }

    if (parameter.first == "requestPayment")
    {
        return false;
    }

    if (parameter.first == "response-cache-control")
    {
        return false;
    }

    if (parameter.first == "response-content-disposition")
    {
        return false;
    }

    if (parameter.first == "response-content-encoding")
    {
        return false;
    }

    if (parameter.first == "response-content-language")
    {
        return false;
    }

    if (parameter.first == "response-content-type")
    {
        return false;
    }

    if (parameter.first == "response-expires")
    {
        return false;
    }

    if (parameter.first == "tagging")
    {
        return false;
    }

    if (parameter.first == "torrent")
    {
        return false;
    }

    if (parameter.first == "uploadId")
    {
        return false;
    }

    if (parameter.first == "uploads")
    {
        return false;
    }

    if (parameter.first == "versionId")
    {
        return false;
    }

    if (parameter.first == "versioning")
    {
        return false;
    }

    if (parameter.first == "versions")
    {
        return false;
    }

    if (parameter.first == "website")
    {
        return false;
    }

    return true;
}

std::string S3Signer::generateResourcePath(const std::string& bucket, const std::string& key)
{
    std::stringstream strbuf;

    if (!bucket.empty())
    {
        strbuf << '/' << bucket;
    }
    
    strbuf << '/';

    if (!key.empty())
    {
        strbuf << key;
    }

    return strbuf.str();
}

S3Signer::S3Signer(const std::string& bucket, const std::string& key) :
        resourcePath(S3Signer::generateResourcePath(bucket, key))
{
}

S3Signer::S3Signer(const std::string& resourcePath) :
        resourcePath(resourcePath)
{
}

S3Signer::~S3Signer()
{
}

RequestHandle S3Signer::operator ()(std::tr1::shared_ptr<AWSCredentials> credentials, std::tr1::shared_ptr<Request> request, std::time_t time) const
{
    return S3Signer::sign(credentials, request, time, this->resourcePath);
}

}

