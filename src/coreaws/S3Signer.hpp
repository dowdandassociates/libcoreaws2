/*
 *
 * libcoreaws2/src/coreaws/S3Signer.hpp
 *
 *-------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS__S3_SIGNER
#define COREAWS__S3_SIGNER

#include "Signer.hpp"

#include <set>

#include "HeaderMap.hpp"
#include "HeaderPair.hpp"
#include "ParameterMap.hpp"
#include "ParameterPair.hpp"

namespace coreaws
{

class S3Signer : public Signer
{
public:
    static RequestHandle sign(AWSCredentialsHandle credentials,
                              RequestHandle request,
                              std::time_t time,
                              const std::string& bucket,
                              const std::string& key);

    static RequestHandle sign(AWSCredentialsHandle credentials,
                              RequestHandle request,
                              std::time_t time,
                              const std::string& resourcePath);

    static std::string generateResourcePath(const std::string& bucket, const std::string& key);

    S3Signer(const std::string& bucket, const std::string& key);
    S3Signer(const std::string& resourcePath);
    virtual ~S3Signer();
    virtual RequestHandle operator ()(AWSCredentialsHandle credentials, RequestHandle request, std::time_t time) const;

    const std::string resourcePath;

protected:
    static std::string getStringToSign(const Method& method,
                                       const HeaderMap& lowerHeaders,
                                       const std::string& resourcePath,
                                       const ParameterMap& parameters);
    static bool isAuthorization(const HeaderPair& header);
    static bool isNotInterestingHeader(const HeaderPair& header);
    static bool isNotCanonicalParameter(const ParameterPair& parameter);
    static HeaderMap getInterestingHeaders(const HeaderMap& headers);
    static std::string buildHeaderSection(const std::string& acc, const HeaderPair& header);
    static std::string getMethodToSign(const Method& method);
    static std::string getHeadersToSign(const HeaderMap& lowerHeaders);
    static std::string getResourceToSign(const std::string& resourcePath, const ParameterMap& parameters);
};

}

#endif // not COREAWS__S3_SIGNER

