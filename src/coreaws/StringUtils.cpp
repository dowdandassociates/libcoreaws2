/*
 *
 * libcoreaws2/src/coreaws/StringUtils.cpp
 *
 *-------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 *
 */

#include "StringUtils.hpp"

#include <algorithm>
#include <cctype>

#include <iostream>

namespace coreaws
{

bool StringUtils::startsWith(const std::string& haystack, const std::string& needle)
{
    return (haystack.find(needle) == 0);
}

bool StringUtils::startsWithIgnoreCase(const std::string& haystack, const std::string& needle)
{
    std::string lowerHaystack = StringUtils::toLowerCase(haystack);
    std::string lowerNeedle = StringUtils::toLowerCase(needle);
    return StringUtils::startsWith(lowerHaystack, lowerNeedle);
}

bool StringUtils::endsWith(const std::string& haystack, const std::string& needle)
{
    if (haystack.length() >= needle.length())
    {
        return (haystack.compare(haystack.length() - needle.length(), needle.length(), needle) == 0);
    }
    else
    {
        return false;
    }
}

bool StringUtils::endsWithIgnoreCase(const std::string& haystack, const std::string& needle)
{
    std::string lowerHaystack = StringUtils::toLowerCase(haystack);
    std::string lowerNeedle = StringUtils::toLowerCase(needle);
    return StringUtils::endsWith(lowerHaystack, lowerNeedle);
}

std::string StringUtils::toLowerCase(const std::string& str)
{
//    std::cerr << "lowercase input: \"" << str << '"' << std::endl;
    std::string lowercase(str.length(), '\0');
    std::transform(str.begin(), str.end(), lowercase.begin(), ::tolower);
//    std::cerr << "lowercase output: \"" << lowercase << '"' << std::endl;
    return lowercase;
}

std::string StringUtils::toUpperCase(const std::string& str)
{
    std::string uppercase(str.length(), '\0');
    std::transform(str.begin(), str.end(), uppercase.begin(), ::toupper);
    return uppercase;
}

}

