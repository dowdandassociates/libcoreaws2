/*
 *
 * libcoreaws2/src/coreaws/UrlUtils.cpp
 *
 *-------------------------------------------------------------------------------
 * Copyright 2012 Dowd and Associates
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *-------------------------------------------------------------------------------
 *
 */

#ifndef COREAWS__URL_UTILS
#define COREAWS__URL_UTILS

#include <string>

namespace coreaws
{

class UrlUtils
{
public:
    static std::string escape(const std::string& str);
    static std::string escapePath(const std::string& path);
private:
    static std::string escapeCharacter(const std::string& acc, char c);
    static std::string escapePathCharacter(const std::string& acc, char c);
    static bool isNotSpecialCharacter(char c);
    static bool isNotSpecialPathCharacter(char c);
};

}

#endif // not COREAWS__URL_UTILS

